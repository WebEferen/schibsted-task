// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAbh9oxwUtzDn5zbt_VGRTpok_qWZGc-Ic',
    authDomain: 'shibsted-task.firebaseapp.com',
    databaseURL: 'https://shibsted-task.firebaseio.com',
    projectId: 'shibsted-task',
    storageBucket: 'shibsted-task.appspot.com',
    messagingSenderId: '236733355202',
    appId: '1:236733355202:web:cb28369fad463e25a6d75b',
    measurementId: 'G-DGJ8J4E3V3'
  }
};
