import React, { useState } from 'react';
import { withFirebase } from './hocs/withFirebase';

import Login from './components/login';
import Dashboard from './components/dashboard';

export const App = props => {
  const [authToken, setAuthToken] = useState<string>(localStorage.getItem('authToken'));

  const setUserToken = (token: string) => {
    localStorage.setItem('authToken', token);
    setAuthToken(token);
  };

  return authToken ? <Dashboard authToken={authToken} /> : <Login auth={props.auth} authHandler={setUserToken} />;
};

export default withFirebase(App);
