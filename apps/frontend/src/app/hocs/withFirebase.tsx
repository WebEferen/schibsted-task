import React from 'react';
import { initializeApp, app, apps, auth, firestore } from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

import { environment } from '../../environments/environment';

export interface WithFirebaseProps {
  auth: auth.Auth;
  firestore: firestore.Firestore;
}

export const withFirebase = <P extends object>(Component: React.ComponentType<P>) =>
  class WithFirebase extends React.Component<P & WithFirebaseProps> {
    constructor(props) {
      !apps.length ? initializeApp(environment.firebase) : app();
      super(props);
    }

    render() {
      const { ...props } = this.props;
      return <Component firestore={firestore()} auth={auth()} {...props as P} />;
    }
  };
