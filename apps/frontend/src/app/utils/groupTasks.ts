import { ColumnModel, StatusModel, TaskModel } from '@schibsted-task/models';

export const groupTasks = (tasks: TaskModel[], statuses: StatusModel[]): ColumnModel[] => {
  const columns = statuses
    .map(status => { return { tasks: [], id: status.id, title: status.title, order: status.order }; })
    .sort((a, b) => (a.order > b.order) ? 1 : -1);

  tasks.forEach(task => {
    const columnIndex = columns.findIndex(column => column.id === task.status);
    if (columnIndex !== -1) columns[columnIndex].tasks.push(task);
  });

  return columns;
};
