import { HttpGetData } from './http';

interface FetchModel<T> {
  data: T;
  status: number;
}

export const useFetch = async <T> (endpoint: string, token: string): Promise<FetchModel<T>> => {
  return await HttpGetData<T>(endpoint, token);
};
