import { catchError } from '@schibsted-task/utils';
import * as http from 'axios';

interface ResponseModel<T> {
  data?: T;
  status: number;
}

export const HttpGetData = async <T> (endpoint: string, token: string): Promise<ResponseModel<T>> => {
  const [result, error] = await catchError(http.default.get(endpoint, { headers: {
      'AuthToken': token.toString()
    }}));

  if (error) {
    const statusCode = Number(error.message.split(' ').reverse().shift());
    return { status: statusCode, data: null };
  }

  return { status: 200, data: result.data.data };
};

export const HttpDeleteData = async <T> (endpoint: string, token: string): Promise<ResponseModel<T>> => {
  const [_, error] = await catchError(http.default.delete(endpoint, { headers: {
      'AuthToken': token.toString()
    }}));

  if (error) {
    const statusCode = Number(error.message.split(' ').reverse().shift());
    return { status: statusCode, data: null };
  }

  return { status: 200 };
};

export const HttpPutData = async <T> (endpoint: string, token: string, data: object): Promise<ResponseModel<T>> => {
  const [result, error] = await catchError(http.default.put(endpoint, data, { headers: {
      'AuthToken': token.toString()
    }}));

  if (error) {
    const statusCode = Number(error.message.split(' ').reverse().shift());
    return { status: statusCode, data: null };
  }

  return { status: 200, data: result.data.data };
};

export const HttpPostData = async <T> (endpoint: string, token: string, data: object): Promise<ResponseModel<T>> => {
  const [result, error] = await catchError(http.default.post(endpoint, data, { headers: {
      'AuthToken': token.toString()
    }}));

  if (error) {
    const statusCode = Number(error.message.split(' ').reverse().shift());
    return { status: statusCode, data: null };
  }

  return { status: 200, data: result.data.data };
};
