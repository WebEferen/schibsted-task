export const clearSession = () => {
  localStorage.removeItem('authToken');
  window.location.reload();
};
