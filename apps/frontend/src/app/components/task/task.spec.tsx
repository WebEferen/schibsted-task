import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';

import { TaskModel } from '@schibsted-task/models';
import { Task } from './task';

const TASK_MOCK: TaskModel = {
  id: 'someId',
  description: 'someDesc',
  title: 'someTitle',
  status: 'open',
  assignee: 'assignee',
  createdAt: new Date(),
  updatedAt: new Date()
};

describe('Task', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should render', () => {
    act(() => {
      ReactDOM.render(<Task task={TASK_MOCK} />, container);
    });

    const h3 = container.querySelector('h3');
    const label = container.querySelector('p');

    expect(label.textContent).toBe(TASK_MOCK.description);
    expect(h3.textContent).toBe(TASK_MOCK.title);
  });
});
