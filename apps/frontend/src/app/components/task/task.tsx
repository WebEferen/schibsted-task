import React from 'react';
import { TaskModel } from '@schibsted-task/models';
import './task.styl';

interface TaskProps {
  task: TaskModel;
  deleteHandler: () => void;
}

export const Task = ({ task, deleteHandler }: TaskProps) => {
  const { id, title, description, status } = task;

  const dragHandler = (event) => {
    event.dataTransfer.setData('text', event.target.id);
  };

  return <div className={'task'} onDragStart={dragHandler} draggable={true} id={id}>
    <div>
      <h3>{ title }</h3>
      { status === 'closed' && <span onClick={deleteHandler}>x</span> }
    </div>
    <article>{ description }</article>
  </div>;
};
