import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import Login from './login';

import { auth } from 'firebase/app';

describe('Login', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should render', () => {
    const authHandler = jest.fn();
    const authMock = {
      signInWithEmailAndPassword: (email: string, password: string) => null
    } as auth.Auth;

    act(() => {
      ReactDOM.render(<Login auth={authMock} authHandler={authHandler} />, container);
    });

    const h1 = container.querySelector('h1');
    expect(h1.textContent).toBe('Login Form:');
  });
});
