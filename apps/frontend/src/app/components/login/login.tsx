import React, { useRef, useState } from 'react';
import { auth } from 'firebase/app';

import { catchError } from '@schibsted-task/utils';
import { Form } from '@schibsted-task/ui';

interface LoginProps {
  auth: auth.Auth;
  authHandler: (token: string) => void;
}

const Login = ({ auth, authHandler }: LoginProps) => {
  const [inputError, setInputError] = useState<string>(null);
  const emailInputRef = useRef<HTMLInputElement>(null);
  const passwordInputRef = useRef<HTMLInputElement>(null);

  const handleLogin = async () => {
    const email = emailInputRef.current.value;
    const password = passwordInputRef.current.value;

    const [userCredential, userError] = await catchError(auth.signInWithEmailAndPassword(email, password));
    if (!userCredential || userError) {
      setInputError(userError.message);
      return;
    }

    const [userToken, tokenError] = await catchError(userCredential.user.getIdToken(true));
    if (!userToken || tokenError) {
      setInputError(tokenError.message);
      return;
    }

    authHandler(userToken);
  };

  return <div>
    <h1>Login Form:</h1> <br />
    <p>
      Credentials: <br />
      Email: dummy@gmail.com <br />
      Password: dummy123
    </p>
    <Form
      handleLogin={handleLogin}
      emailRef={emailInputRef}
      passwordRef={passwordInputRef}
      inputError={inputError}
    />
  </div>;
};

export default Login;
