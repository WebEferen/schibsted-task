import React, { useEffect, useState } from 'react';
import { ColumnModel, TaskRequestModel } from '@schibsted-task/models';
import { Navbar, ActionButton, ToastMessage } from '@schibsted-task/ui';

import { clearSession, HttpPostData, HttpPutData, HttpGetData, HttpDeleteData } from '../../utils';
import { Column, AddTaskDialog } from './components';

import './dashboard.styl';

interface DashboardProps {
  authToken: string;
}

const Dashboard = ({ authToken }: DashboardProps) => {
  const [toastMessage, setToastMessage] = useState<{ type: string, message: string }>(null);
  const [freshState, setFreshState] = useState<boolean>(false);
  const [addTaskVisible, setAddTaskVisible] = useState<boolean>(false);
  const [columns, setColumns] = useState<ColumnModel[]>(null);

  useEffect(() => {
    if (!freshState) {
      HttpGetData<ColumnModel[]>('/api/tasks', authToken).then((tasks) => {
        if (tasks.status === 401) return clearSession();
        setColumns(tasks.data);
        setFreshState(true);
      });
    }
  }, [freshState, authToken]);

  const addTaskHandler = async (task: TaskRequestModel) => {
    const { status } = await HttpPostData<TaskRequestModel>(`/api/tasks`, authToken, task);
    setAddTaskVisible(false);

    if (status === 200) {
      setToastMessage({ type: 'success', message: 'Successfully added task!' });
      setFreshState(false);
      return;
    }

    setToastMessage({ type: 'error', message: 'Unable to add task!' });
    return;
  };

  const dropHandler = async ({ id, taskId }): Promise<boolean> => {
    const { status } = await HttpPutData(`/api/task/${taskId}`, authToken, { status: id });
    return status === 200;
  };

  const deleteHandler = async (id: string): Promise<boolean> => {
    const { status } = await HttpDeleteData(`/api/task/${id}`, authToken);
    if (status === 200) {
      setToastMessage({ type: 'success', message: 'Successfully deleted task!' });
      setFreshState(false);
      return true;
    }

    setToastMessage({ type: 'error', message: 'Unable to delete task!' });
    return false;
  };

  const ColumnGrid = () => {
    return <main className={'main'}>
      { columns ? columns.map(column => <Column
        {...column}
        key={column.id}
        deleteHandler={deleteHandler}
        dropHandler={dropHandler}
        dropStatusHandler={(success) => {
          success ?
            setToastMessage({ type: 'success', message: 'Successfully changed status!' }) :
            setToastMessage({ type: 'error', message: 'Unable to change status!' });

          setFreshState(false);
        }}
      />) : null }
    </main>;
  };

  return <React.Fragment>
    <Navbar onLogoutHandler={() => clearSession()} />
    <ColumnGrid />

    <AddTaskDialog
      hasClose={true}
      isVisible={addTaskVisible}
      addHandler={addTaskHandler}
      cancelHandler={() => setAddTaskVisible(false)}
    />

    <ActionButton clickHandler={() => setAddTaskVisible(true)} />
    <ToastMessage delayInMs={3000} toast={toastMessage} />
  </React.Fragment>;
};

export default Dashboard;
