import React from 'react';
import { TaskModel } from '@schibsted-task/models';
import { Task } from '../../../task';
import './column.styl';

export interface ColumnProps {
  id: string;
  title: string;
  tasks: TaskModel[];

  dropHandler: ({ id, taskId }: { id: string, taskId: string }) => Promise<boolean>;
  dropStatusHandler: (status: boolean) => void;
  deleteHandler: (id: string) => void;
}

export const Column = (props: ColumnProps) => {
  const { id, tasks, title, dropHandler, dropStatusHandler, deleteHandler } = props;

  const onDropHandler = async (event) => {
    event.preventDefault();
    event.persist();

    const taskId = event.dataTransfer.getData('text');
    const response = await dropHandler({ id, taskId });

    if (response) {
      event.target.appendChild(document.getElementById(taskId));
      return dropStatusHandler(true);
    }

   return dropStatusHandler(false);
  };

  return <div className={'column'} onDrop={onDropHandler} onDragOver={(ev) => ev.preventDefault()}>
    <h1>Column: {title}</h1>

    <div id={id}>
      { tasks.map(task => <Task key={task.id} deleteHandler={() => deleteHandler(task.id)} task={task} />) }
    </div>
  </div>;
};
