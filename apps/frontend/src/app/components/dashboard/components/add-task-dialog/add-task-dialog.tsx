import React, { useRef } from 'react';
import { Modal } from '@schibsted-task/ui';
import { TaskRequestModel } from '@schibsted-task/models';

import './add-task-dialog.styl';

interface AddTaskDialogProps {
  hasClose?: boolean;
  isVisible: boolean;
  addHandler: (task: TaskRequestModel) => void;
  cancelHandler: () => void;
}

export const AddTaskDialog = (props: AddTaskDialogProps) => {
  const titleInputRef = useRef<HTMLInputElement>(null);
  const descriptionInputRef = useRef<HTMLTextAreaElement>(null);

  const { isVisible, hasClose, addHandler, cancelHandler } = props;

  const onAddHandler = () => {
    const title = titleInputRef.current.value;
    const description = descriptionInputRef.current.value;
    if (!title || !description) return;

    addHandler({ title, description });
  };

  return <Modal id={'add-task-modal'} isVisible={isVisible} isShadowed={true} isCentered={true}>
    <div className={'add-task-dialog'}>
      <div className={'dialog-header'}>
        <span>Add new issue</span>
        { hasClose && <span onClick={cancelHandler} className={'dialog-close'}>x</span>}
      </div>
      <div className={'dialog-content'}>
          <section>
            <label htmlFor={'title'}>Issue title: </label><br />
            <input id={'title'} type={'text'} name={'title'} ref={titleInputRef} />
          </section>
          <section>
            <label htmlFor={'description'}>Issue description: </label><br />
            <textarea id={'description'} name={'description'} ref={descriptionInputRef} />
          </section>
      </div>
      <div className={'dialog-actions'}>
        <button onClick={onAddHandler} className={'add'}>Add</button>
        <button onClick={cancelHandler}>Cancel</button>
      </div>
    </div>
  </Modal>;
};
