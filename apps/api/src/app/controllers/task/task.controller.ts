import { Request, Response } from 'express';
import { MissingPropertiesError } from '@schibsted-task/models';

import { Controller } from '../controller';
import { TasksService } from '../../services/tasks';

export class TaskController extends Controller {
  static ROUTE = '/api/task/:id';

  static async get(request: Request, response: Response) {
    const { id } = request.params;
    if (!id) return super.errorResponse(response, MissingPropertiesError(['id']));

    const tasksService = new TasksService(request.header('authToken'));
    const [task, error] = await tasksService.getTask(id);

    if (error) return super.errorResponse(response, error);
    return super.successResponse(response, task);
  }

  static async put(request: Request, response: Response) {
    const { id } = request.params;
    const { status } = request.body;

    if (!id || !status) return super.errorResponse(response, MissingPropertiesError(['id', 'status']));

    const tasksService = new TasksService(request.header('authToken'));
    const [task, error] = await tasksService.updateTaskStatus(id, status);

    if (error) return super.errorResponse(response, error);
    return super.successResponse(response, task);
  }

  static async delete(request: Request, response: Response) {
    const { id } = request.params;
    if (!id) return super.errorResponse(response, MissingPropertiesError(['id']));

    const tasksService = new TasksService(request.header('authToken'));
    const [task, error] = await tasksService.deleteTask(id);

    if (error) return super.errorResponse(response, error);
    return super.successResponse(response, task);
  }
}
