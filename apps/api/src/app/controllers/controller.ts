import { Request, Response } from 'express';
import { ErrorModel, errorResponse, successResponse } from '@schibsted-task/models';

export abstract class Controller {
  static ROUTE = '/api';

  static async get(request: Request, response: Response): Promise<Response> {
    return response.status(405).send('Method Not Found');
  };

  static async post(request: Request, response: Response): Promise<Response> {
    return response.status(405).send('Method Not Found');
  }

  static async put(request: Request, response: Response): Promise<Response> {
    return response.status(405).send('Method Not Found');
  }

  static async delete(request: Request, response: Response): Promise<Response> {
    return response.status(405).send('Method Not Found');
  }

  static async patch(request: Request, response: Response): Promise<Response> {
    return response.status(405).send('Method Not Found');
  }

  static errorResponse(response: Response, error: ErrorModel): Response {
    return response.status(error.status).send(errorResponse(error));
  };

  static successResponse(response: Response, data, status = 200): Response {
    return response.status(status).send(successResponse(data));
  }

}
