import { Request, Response } from 'express';
import { ErrorFactory } from '@schibsted-task/models';

import { Controller } from '../controller';
import { StatusesService } from '../../services/statuses';

export class StatusesController extends Controller {
  static ROUTE = '/api/statuses';

  static async get(request: Request, response: Response) {
    const statusesService = new StatusesService(request.header('AuthToken'));
    const statuses = await statusesService.getStatuses();

    if (!statuses) return super.errorResponse(response, ErrorFactory('Statuses not found', 403));
    return super.successResponse(response, statuses);
  }
}
