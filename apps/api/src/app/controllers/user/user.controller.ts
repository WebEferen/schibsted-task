import { Request, Response } from 'express';
import { catchError } from '@schibsted-task/utils';
import { InternalServerError } from '@schibsted-task/models';

import { Controller } from '../controller';
import { AuthService } from '../../services/auth';

export class UserController extends Controller {
  static ROUTE = '/api/user';

  static async get(request: Request, response: Response) {
    const authToken = request.header('AuthToken');
    const authService = new AuthService(authToken);

    const [user, error] = await catchError(authService.getCurrentUser());
    if (error || !user) return super.errorResponse(response, InternalServerError('[UserController] Something went wrong'));

    return super.successResponse(response, user);
  }
}
