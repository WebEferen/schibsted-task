import { Request, Response } from 'express';
import { Controller } from '../controller';

export class PingController extends Controller {
  static ROUTE = '/api';

  static async get(request: Request, response: Response) {
    return super.successResponse(response, 'Pong!');
  }
}
