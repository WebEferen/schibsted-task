import { Request, Response } from 'express';
import { MissingPropertiesError } from '@schibsted-task/models';

import { Controller } from '../controller';
import { TasksService } from '../../services/tasks';

export class TasksController extends Controller {
  static ROUTE = '/api/tasks';

  static async get(request: Request, response: Response) {
    const tasksService = new TasksService(request.header('AuthToken'));
    const [tasks, error] = await tasksService.getGroupedTasks();

    if (error) return super.errorResponse(response, error);
    return super.successResponse(response, tasks);
  }

  static async post(request: Request, response: Response) {
    const { title, description } = request.body;
    if (!title || !description)
      return super.errorResponse(response, MissingPropertiesError(['title', 'description']));

    const tasksService = new TasksService(request.header('authToken'));
    const [task, error] = await tasksService.addTask(title, description);

    return !error ?
      super.successResponse(response, task, 201) :
      super.errorResponse(response, error);
  }
}
