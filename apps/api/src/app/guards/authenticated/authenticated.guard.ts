import { Request, Response } from 'express';
import { catchError } from '@schibsted-task/utils';
import { UnauthorizedError } from '@schibsted-task/models';
import { FirebaseService } from '../../services/firebase';

export class AuthenticatedGuard {
  static async isAllowed(request: Request, response: Response, next: VoidFunction) {
    const authToken = request.header('AuthToken');
    if (!authToken) return response.status(401).send(UnauthorizedError('AuthenticatedGuard'));

    const firebaseService = new FirebaseService(authToken);
    const [isAllowed, error] = await catchError(firebaseService.verifyToken());
    return isAllowed && !error ? next() : response.status(401).send(UnauthorizedError('AuthenticatedGuard'));
  }
}
