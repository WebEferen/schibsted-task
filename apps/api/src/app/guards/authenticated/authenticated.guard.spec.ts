import { Request, Response } from 'express';
import { AuthenticatedGuard } from './authenticated.guard';

describe('Authenticated Guard', () => {
  let requestMock: Request;
  let responseMock: Response;

  beforeEach(() => {
    requestMock = {
      header: (name: string) => name,
      headers: {},
      body: {}
    } as Request;

    responseMock = {
      status: (code: number) => code && responseMock,
      send: (body) => body
    } as Response;

    jest.spyOn(AuthenticatedGuard, 'isAllowed')
      .mockImplementation((req, res, next) => new Promise((resolve) => {
        if (req.headers.authToken) return resolve(next());
        return resolve(res.send());
      }));

  });

  it('should pass when token provided', async () => {
    const nextSpy = jest.fn();
    requestMock.headers.authToken = 'SomeToken';

    await AuthenticatedGuard.isAllowed(requestMock, responseMock, nextSpy);
    expect(nextSpy).toHaveBeenCalled();
  });

  it('should NOT pass when token NOT provided', async () => {
    const nextSpy = jest.fn();

    await AuthenticatedGuard.isAllowed(requestMock, responseMock, nextSpy);
    expect(nextSpy).not.toHaveBeenCalled();
  });

});
