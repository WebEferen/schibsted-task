import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import { UserModel } from '@schibsted-task/models';

describe('AuthService', () => {
  let requestMock: Request;
  let responseMock: Response;

  beforeEach(() => {
    requestMock = {
      header: (name: string) => name,
      headers: {},
      body: {}
    } as Request;

    responseMock = {
      status: (code: number) => code && responseMock,
      send: (body) => body
    } as Response;
  });

  it('should get current user', async () => {
    const authService = new AuthService('');
    const userModel: UserModel = { id: 'someId', email: '', avatar: '', firstName: '', lastName: '', createdAt: new Date(), updatedAt: new Date() };

    jest.spyOn(authService.firebaseService, 'getCurrentUser').mockResolvedValue(userModel);

    const userResult = await authService.getCurrentUser();
    expect(userResult).toBe(userModel);
  });
});
