import { catchError } from '@schibsted-task/utils';
import { UserModel } from '@schibsted-task/models';
import { FirebaseService } from '../firebase';

export class AuthService {
  public readonly firebaseService: FirebaseService;

  constructor(authToken: string) {
    this.firebaseService = new FirebaseService(authToken);
  }

  public async getCurrentUser(): Promise<UserModel | null> {
    const [user, error] = await catchError(this.firebaseService.getCurrentUser());
    return !user || error ? null : user;
  }
}
