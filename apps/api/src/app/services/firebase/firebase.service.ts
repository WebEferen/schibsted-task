import * as firebase from 'firebase-admin';
import { firestore } from 'firebase/app';
import { environment } from '../../../environments/environment';

import { catchError } from '@schibsted-task/utils';
import { UserModel } from '@schibsted-task/models';

firebase.initializeApp({
  credential: firebase.credential.cert(environment.firebase),
  databaseURL: "https://shibsted-task.firebaseio.com"
});

export interface QueryFilterOption {
  field: string;
  operator: firestore.WhereFilterOp;
  value: string | number;
}

export class FirebaseService {
  public readonly auth: firebase.auth.Auth;
  public readonly firestore: firebase.firestore.Firestore;
  private readonly authToken: string;

  constructor(authToken: string) {
    this.authToken = authToken;
    this.firestore = firebase.firestore();
    this.auth = firebase.auth();
  }

  public async verifyToken() {
    const isCorrect = await this.getDecodedToken();
    return !!isCorrect;
  }

  public async getDecodedToken(): Promise<firebase.auth.DecodedIdToken | null> {
    const [decodedToken] = await catchError(this.auth.verifyIdToken(this.authToken));
    return decodedToken && decodedToken.uid ? decodedToken : null;
  }

  public async getCurrentUser(): Promise<UserModel | null> {
    const { uid } = await this.getDecodedToken();
    const [user, userError] = await catchError(this.getDocument('users', uid).get());

    return user && user.exists && !userError ? user.data() as UserModel : null;
  }

  public getCollection(collection: string): firebase.firestore.CollectionReference {
    return this.firestore.collection(collection);
  }

  public getDocument(collection: string, document: string): firebase.firestore.DocumentReference {
    return this.getCollection(collection).doc(document);
  }

  public getFilteredDocuments(collection: string, filters: QueryFilterOption[] = [], limit?: number): firebase.firestore.Query {
    const collectionRef = this.getCollection(collection);
    const { field, value, operator } = filters.shift();

    if (!field || !value || !operator) {
      return limit ? collectionRef.limit(limit) : collectionRef;
    }

    let query = collectionRef.where(field, operator, value);
    filters.forEach(filter => query = query.where(filter.field, filter.operator, filter.value));

    return limit ? query.limit(limit) : query;
  }
}
