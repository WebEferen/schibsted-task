import { Request, Response } from 'express';
import * as firebase from 'firebase-admin';
import { FirebaseService } from './firebase.service';

describe('FirebaseService', () => {
  const firebaseService = new FirebaseService('');
  let requestMock: Request;
  let responseMock: Response;

  beforeEach(() => {
    requestMock = {
      header: (name: string) => name,
      headers: {},
      body: {}
    } as Request;

    responseMock = {
      status: (code: number) => code && responseMock,
      send: (body) => body
    } as Response;
  });

  describe('token', () => {
    it('should get decoded token', async () => {
      jest.spyOn(firebaseService.auth, 'verifyIdToken').mockResolvedValue({ uid: 'someId' } as firebase.auth.DecodedIdToken);
      const token = await firebaseService.getDecodedToken();

      expect(token).toBeDefined();
      expect(token.uid).toBe('someId');
    });

    it ('should NOT get decoded token', async () => {
      jest.spyOn(firebaseService.auth, 'verifyIdToken').mockResolvedValue(null);

      const missingToken = await firebaseService.getDecodedToken();
      expect(missingToken).toBeNull();
    });

    it('should verify token', async () => {
      jest.spyOn(firebaseService, 'getDecodedToken').mockResolvedValue({ uid: 'someId' } as firebase.auth.DecodedIdToken);

      const verifiedToken = await firebaseService.verifyToken();
      expect(verifiedToken).toBeTruthy();
    });

    it('should NOT verify token', async () => {
      jest.spyOn(firebaseService, 'getDecodedToken').mockResolvedValue(null);

      const verifiedToken = await firebaseService.verifyToken();
      expect(verifiedToken).toBeFalsy();
    });
  });

  describe('collections', () => {

    it('should get collection', async () => {
      jest.spyOn(firebaseService.firestore, 'collection').mockImplementation((collection: string) => {
        return { id: collection } as firebase.firestore.CollectionReference;
      });

      const collection = await firebaseService.getCollection('someCollection');
      expect(collection.id).toBe('someCollection');
    });

    it('should NOT get collection', async () => {
      jest.spyOn(firebaseService.firestore, 'collection').mockImplementation((collection: string) => {
        return undefined as firebase.firestore.CollectionReference;
      });

      const collection = await firebaseService.getCollection('someNotExistingCollection');
      expect(collection).toBeUndefined();
    });
  });



});
