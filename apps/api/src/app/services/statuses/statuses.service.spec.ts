import { Request, Response } from 'express';
import * as firebase from 'firebase-admin';
import { StatusesService } from './statuses.service';
import { StatusModel } from '@schibsted-task/models';

describe('StatusesService', () => {
  let requestMock: Request;
  let responseMock: Response;

  beforeEach(() => {
    requestMock = {
      header: (name: string) => name,
      headers: {},
      body: {}
    } as Request;

    responseMock = {
      status: (code: number) => code && responseMock,
      send: (body) => body
    } as Response;
  });

  it('should get status reference', async () => {
    const statusesService = new StatusesService('');
    const statusModel: StatusModel = { id: 'open', title: 'Open', transitions: [], initial: true, order: 1 };

    jest.spyOn(statusesService.firebaseService, 'getCollection').mockImplementation((collection: string) => {
      return { get: () => new Promise((resolve, reject) => {
          collection === 'statuses' ? resolve({ docs: [{ exists: true, data: () => statusModel }] } as any) : reject();
        })} as firebase.firestore.CollectionReference;
    });

    const statuses = await statusesService.getStatuses();
    expect(statuses.length).toBe(1);
    expect(statuses[0]).toBe(statusModel);
  });
});
