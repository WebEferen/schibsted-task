import { catchError } from '@schibsted-task/utils';
import { StatusModel } from '@schibsted-task/models';
import { FirebaseService } from '../firebase';


export class StatusesService {
  public readonly firebaseService: FirebaseService;

  constructor(authToken: string) {
    this.firebaseService = new FirebaseService(authToken);
  }

  public async getStatuses(): Promise<StatusModel[]> {
    const [statuses, statusesError] = await catchError(this.firebaseService.getCollection('statuses').get());
    if (!statuses || statusesError) return [];

    return statuses.docs.filter(doc => doc.exists).map(doc => doc.data() as StatusModel);
  }
}
