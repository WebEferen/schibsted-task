import { v4 as uuid4 } from 'uuid';
import { catchError } from '@schibsted-task/utils';
import {
  BadRequestError,
  ErrorFactory,
  UnauthorizedError,
  TaskModel,
  TupleResponse, ColumnModel
} from '@schibsted-task/models';

import { FirebaseService } from '../firebase';
import { StatusesService } from '../statuses';
import { AuthService } from '../auth';


export class TasksService {
  private readonly firebaseService: FirebaseService;
  private readonly statusesService: StatusesService;
  private readonly authService: AuthService;

  constructor(authToken: string) {
    this.firebaseService = new FirebaseService(authToken);
    this.statusesService = new StatusesService(authToken);
    this.authService = new AuthService(authToken);
  }

  public async getGroupedTasks(): TupleResponse<ColumnModel[]> {
    const [tasks, tasksError] = await this.getTasks();
    const statuses = await this.statusesService.getStatuses();
    if (tasksError) return [null, tasksError];

    const taskColumns: ColumnModel[] = statuses
      .map(status => { return { tasks: [], id: status.id, title: status.title, order: status.order }; })
      .sort((a, b) => (a.order > b.order) ? 1 : -1);

    tasks.forEach(task => {
      const columnIndex = taskColumns.findIndex(column => column.id === task.status);
      if (columnIndex !== -1) taskColumns[columnIndex].tasks.push(task);
    });

    return [taskColumns, null];
  }

  public async getTasks(): TupleResponse<TaskModel[]> {
    const { id: userId } = await this.authService.getCurrentUser();
    if(!userId) return [null, UnauthorizedError('TasksService')];

    const tasksReference = this.firebaseService.getFilteredDocuments('tasks', [{ field: 'assignee', operator: '==', value: userId }]);
    const [tasks, tasksError] = await catchError(tasksReference.get());
    if (tasksError) return [null, ErrorFactory('Entity getting error', 403)];

    const parsedTasks = tasks.docs.filter(doc => doc.exists).map(doc => doc.data() as TaskModel);

    return [parsedTasks, null];
  }

  public async updateTaskStatus(id: string, status: string): TupleResponse<TaskModel> {
    const { id: userId } = await this.authService.getCurrentUser();
    if(!userId) return [null, UnauthorizedError('TasksService')];

    const taskReference = await this.getTaskReference(id, userId);
    if (!taskReference || !taskReference.data())
      return [null, ErrorFactory(`Task with given id not found`, 400)];

    const oldTask = taskReference.data() as TaskModel;

    const statuses = await this.statusesService.getStatuses();
    const newStatus = statuses.find(s => s.id === status);
    const oldStatus = statuses.find(s => s.id === oldTask.status);

    if (!oldStatus || !newStatus)
      return [null, ErrorFactory(`Status literal not found`, 400)];

    if (!oldStatus.transitions.includes(newStatus.id))
      return [null, ErrorFactory(`Status transition not allowed: ${oldStatus.id} -> ${newStatus.id}`, 403)];

    const updatedTask = {...taskReference.data(), status: newStatus.id, updatedAt: new Date() } as TaskModel;
    await taskReference.ref.update(updatedTask);

    return [updatedTask, null];
  }

  public async getTask(id: string): TupleResponse<TaskModel> {
    const { id: userId } = await this.authService.getCurrentUser();
    if(!userId) return [null, UnauthorizedError('TasksService')];

    const taskReference = await this.getTaskReference(id, userId);
    if (!taskReference || !taskReference.data())
      return [null, ErrorFactory(`Task with given id not found`, 400)];

    const task = taskReference.data() as TaskModel;
    return [task, null];
  }

  public async deleteTask(id: string): TupleResponse<string> {
    const { id: userId } = await this.authService.getCurrentUser();
    if(!userId) return [null, BadRequestError('TasksService')];

    const taskReference = await this.getTaskReference(id, userId);
    await taskReference.ref.delete();

    return [id, null];
  }

  public async addTask(title: string, description: string): TupleResponse<TaskModel> {
    const { id: userId } = await this.authService.getCurrentUser();
    if(!userId) return [null, UnauthorizedError('TasksService')];

    const task = await this.taskFactory(title, description, userId);
    await this.firebaseService.getCollection('tasks').doc(task.id).set(task);

    return [task, null];
  }

  private async taskFactory(title: string, description: string, assignee: string): Promise<TaskModel> {
    const statuses = await this.statusesService.getStatuses();
    const initialStatus = statuses.filter(status => status.initial).shift();
    const statusId = !initialStatus ? 'open' : initialStatus.id;

    return {
      title,
      description,
      id: uuid4(),
      status: statusId,
      assignee: assignee,
      createdAt: new Date(),
      updatedAt: new Date()
    };
  }

  private async getTaskReference(id: string, userId: string) {
    const [tasks, tasksError] = await catchError(this.firebaseService.getFilteredDocuments('tasks', [
      { field: 'assignee', operator: '==', value: userId },
      { field: 'id', operator: '==', value: id }
    ]).get());

    return tasksError || !tasks ? null : tasks.docs.filter(doc => doc.exists).shift();
  }
}
