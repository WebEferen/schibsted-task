export const environment = {
  production: true,
  baseRoute: '/api',
  firebase: {
    type: "service_account",
    projectId: "shibsted-task",
    privateKeyId: "e1b2fa2a6af14d68036e2c92023c9a2469f57be3",
    privateKey: "-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCVFSHK0AA+zfYB\n/BQkjEfEAVdDFv/HZmFYviMojO49unJmDySGnTprwcJoptIFeILgbMMRd102tKtU\n9lDBu0CTCLglBW/H50lMwz/fkTzBdpaH8QIEdUW8ukeKTWFcOrRV7EJmGxTw/ZXm\nS1U2cK69LbQNwgjFE0/dW5E6+nedrT8Hj0kfRaEWd/BOVNsCSLq3CNVw0RUquOEG\nER06PFxq4zI4ocKlx3BWQd4MEe8LAN4o/+MzsCaa6f9kpNX4CUxuFdknp4HvEZ0u\n5tt3CidcITO0RJ3VvtcoMe3vlhJysExwAJbUht5Oab4WAG9Kw7L4bOd2oTEizqg0\n7dK4wDO5AgMBAAECgf8EqKRlshpP3B/T8AyxF7EmbuRLNGzJzmk8XLS8j6riQvFQ\nPyjlDATHWUa5/nyRrzJn0P5HjXNcQkkYXponc7KQ5bTbc1AK7y9bNr7IPcGoSgxq\nMx3dBc0oGKojqEdimnMIPFnW1wHQhhwpFjGTgfHz93mo9GdRwUi9oh7nuOwEAVm4\nUw2wvCsKYmEJn3ULn7thPtBjUe7vjqQ4ugYmdFzXEKH+j2JIISDoGcyfOF2CkP6y\nrRydZiG/GB5zuQOqneKkRm5WrVQlcIe8u7VM3izEC0VbttYeVwqHKTnxYwu7TyFe\nfEyIbSSIZaEjgGYFvyUp7MvPBi6gWgJtGtL5WSECgYEA0pgqWPgMbbJovODVdVUS\nasw6W+vPSrQxW24Aa/HVCPxY70gKlFCz6w369JLBpFNL0z/oBukis5XldgijQQtB\nD39TMzFfEQbV+3enyxd5FpbZ5QBf3EzkfaJNRVKDBSgEslMwIQv9h19taT0kPGM2\nJVYiqkt2AXhu0ym2mab9HOMCgYEAtTnML639upJhxe1vkEqdb8uIRUC3gkbVDKxI\n1TkW6gDbIQprE2SmZCDwUliws1nk4nOd8vo1jl3I1+ESqHiWlKmgUlLTFo+l/A7Q\nz00h3H5GIdRehuM1Pb9shUlzaX9QBGkbFTFTuD23I7SrAiLoMIALhbDretlLfCeP\nNow/y7MCgYAbtKUtm0SXUfdxkerDSvPnVQMAgN9+poIv0yL/EIvEaN6SbFnn9U4W\nSHqRnpUXN1YsbqXp8tX75maPxscjkYV3snqRq5ckOFACi1Vp5KarkCEOfaQe6YbN\n+IJN8nMGRKOJimQ0lM0/+A/+3mpb/3lqJBDfcpoNy3hZE43QXSXDnQKBgEn8osQE\ngMYe5RPtb9K047m0mpnLZiAl0u0tmT6T5Cr81nmSy0xLlzLhz7uOwfU3Ch75nM6K\nIDtTRp/8H6XwNWnUJsP1VICtfNJ/Kfi4QM7ILtG45i6jrCTddeykpB26AG0MjCYG\n0WsypwZIyFhC8BB89jdKl5i8BflWHT3j3FfJAoGBAL2xlNgSwkLbeIyD1UIjcaIU\nkejwD5S6g6hoKfSd//cTUy+rPiMKCSjfU+omaRr3hLwT/TnIwA5QX5xSWR4kdF5J\n25CBPce+GKDET1isM2jTzSCARawtTwV6d83yJczYoQH7U6l6nfiSiYbmDs9hoHKq\nkibV9f7hnbLOlwZD3aAG\n-----END PRIVATE KEY-----\n",
    clientEmail: "firebase-adminsdk-s1g00@shibsted-task.iam.gserviceaccount.com",
    clientId: "103324594197144498697",
    authUri: "https://accounts.google.com/o/oauth2/auth",
    tokenUri: "https://oauth2.googleapis.com/token",
    authProviderX509CertUrl: "https://www.googleapis.com/oauth2/v1/certs",
    clientX509CertUrl: "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-s1g00%40shibsted-task.iam.gserviceaccount.com"
  }
};
