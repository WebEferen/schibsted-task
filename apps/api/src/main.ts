import * as express from 'express';
import { json } from 'body-parser';

import { PingController } from './app/controllers/ping';
import { TaskController } from './app/controllers/task';
import { TasksController } from './app/controllers/tasks';
import { UserController } from './app/controllers/user';

import { AuthenticatedGuard } from './app/guards/authenticated';
import { StatusesController } from './app/controllers/statuses';

const app = express();
app.use(json());

// Ping
app.get(PingController.ROUTE, PingController.get);

// Tasks display / add
app.use(TasksController.ROUTE, AuthenticatedGuard.isAllowed);
app.get(TasksController.ROUTE, TasksController.get);
app.post(TasksController.ROUTE, TasksController.post);

// Task management
app.use(TaskController.ROUTE, AuthenticatedGuard.isAllowed);
app.get(TaskController.ROUTE, TaskController.get);
app.put(TaskController.ROUTE, TaskController.put);
app.delete(TaskController.ROUTE, TaskController.delete);

// Statuses display
app.use(StatusesController.ROUTE, AuthenticatedGuard.isAllowed);
app.get(StatusesController.ROUTE, StatusesController.get);

// User info display
app.use(UserController.ROUTE, AuthenticatedGuard.isAllowed);
app.get(UserController.ROUTE, UserController.get);



const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log('Listening at http://localhost:' + port + '/api');
});

server.on('error', console.error);
