export function catchError<T>(promise: Promise<T>): Promise<[T | null, Error | null]> {
  return new Promise(resolve => {
    promise
      .then(data => resolve([data, null]))
      .catch(error => resolve([null, error]));
  });
}
