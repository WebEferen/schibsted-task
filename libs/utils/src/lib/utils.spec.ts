import { catchError } from './utils';

describe('catchError', () => {
  it('should resolve', async () => {
    const [result, error] = await catchError(Promise.resolve(1));
    expect(result).toEqual(1);
    expect(error).toBe(null);
  });

  it('should reject', async () => {
    const [result, error] = await catchError(Promise.reject(1));
    expect(result).toEqual(null);
    expect(error).toBe(1);
  });
});
