export interface ErrorModel {
  message: string;
  status: number;
}

export const ErrorFactory = (message: string, status = 500): ErrorModel => {
  return { status, message };
};

export const UnauthorizedError = (service: string) => {
  return ErrorFactory(`[${service}] Unauthorized`, 401);
};

export const BadRequestError = (service: string) => {
  return ErrorFactory(`[${service}] Bad Request`, 400);
};

export const MissingPropertiesError = (properties: string[]) => {
  return ErrorFactory(`Missing properties: ${properties.join(', ')}`);
};

export const InternalServerError = (service: string) => {
  return ErrorFactory(`[${service}] Internal Server Error`);
};
