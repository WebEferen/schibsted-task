export interface TaskRequestModel {
  title: string;
  description: string;
}

export interface TaskModel extends TaskRequestModel {
  id: string;

  status: string;
  assignee: string;

  createdAt: Date;
  updatedAt: Date;
}
