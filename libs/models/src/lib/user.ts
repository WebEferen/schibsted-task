import { MetadataModel } from './metadata';

export interface UserModel extends MetadataModel {
  id: string;
  avatar: string;
  firstName: string;
  lastName: string;
  email: string;
}
