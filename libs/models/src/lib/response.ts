import { ErrorModel } from './error';
export type TupleResponse<T> = Promise<[T | null, ErrorModel | null]>;

export const errorResponse = ({ message }) => {
  return { success: false, message };
};

export const successResponse = data => {
  return { success: true, data };
};
