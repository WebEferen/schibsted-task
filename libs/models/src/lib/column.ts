import { TaskModel } from '@schibsted-task/models';

export interface ColumnModel {
  id: string;
  title: string;
  tasks: TaskModel[];
}
