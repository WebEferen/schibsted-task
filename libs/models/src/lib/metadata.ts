export interface MetadataModel {
  createdAt: Date;
  updatedAt: Date;
}
