export interface StatusModel {
  id: string;
  title: string;
  transitions: string[];
  order: number;
  initial?: boolean;
}
