import React from 'react';
import './action-button.styl';

interface ActionButtonProps {
  clickHandler: () => void;
}

export const ActionButton = ({ clickHandler }: ActionButtonProps) => {
  return <button onClick={clickHandler} className={'action-button'} />;
};
