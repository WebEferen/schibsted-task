import { act } from 'react-dom/test-utils';
import ReactDOM from 'react-dom';
import React from 'react';

import { ActionButton } from './action-button';



describe('Action Button', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should render', () => {
    act(() => {
      ReactDOM.render(<ActionButton clickHandler={() => null} />, container);
    });

    const button = container.querySelector('button');
    expect(button.className).toBe('action-button');
  });

  it('should emit click', () => {
    const clickHandler = jest.fn();

    act(() => {
      ReactDOM.render(<ActionButton clickHandler={clickHandler} />, container);
    });

    const button = container.querySelector('button');
    expect(button.className).toBe('action-button');

    act(() => {
      button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });
    
    expect(clickHandler).toHaveBeenCalled();
  });
});
