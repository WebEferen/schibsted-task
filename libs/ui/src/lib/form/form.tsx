import React, { Ref } from 'react';

export interface FormProps {
  emailRef: Ref<HTMLInputElement>;
  passwordRef: Ref<HTMLInputElement>;
  handleLogin: () => void;

  inputError?: string;
}

export const Form = (props: FormProps) => {
  return <form onSubmit={e => e.preventDefault()}>
    <span>{props.inputError}</span> <br />
    <input type={'email'} name={'email'} placeholder={'Email'} ref={props.emailRef} /> <br />
    <input type={'password'} name={'password'} placeholder={'Password'} ref={props.passwordRef} />
    <button onClick={props.handleLogin}>Login!</button>
  </form>;
};
