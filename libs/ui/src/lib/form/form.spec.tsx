import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Form } from './form';
import React, { createRef } from 'react';
import createSpy = jasmine.createSpy;

describe('Form', () => {
  let container;
  const emailRef = createRef<HTMLInputElement>();
  const passwordRef = createRef<HTMLInputElement>();

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should render', () => {
    act(() => {
      ReactDOM.render(<Form passwordRef={passwordRef} emailRef={emailRef} handleLogin={() => null} />, container);
    });

    const button = container.querySelector('button');
    const span = container.querySelector('span');

    expect(span.textContent).toBe('');
    expect(button).toBeDefined();
  });

  it('should emit form on click', () => {
    const handleLogin = jest.fn();

    act(() => {
      ReactDOM.render(<Form passwordRef={passwordRef} emailRef={emailRef} handleLogin={handleLogin} />, container);
    });

    const button = container.querySelector('button');
    const [emailInput, passwordInput] = container.querySelectorAll('input');

    act(() => {
      passwordRef.current.value = 'password';
      emailRef.current.value = 'email@email.com';

      button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(handleLogin).toHaveBeenCalled();

    expect(emailInput.value).toBe(emailRef.current.value);
    expect(passwordInput.value).toBe(passwordRef.current.value);
  });

});
