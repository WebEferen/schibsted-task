import React from 'react';
import './navbar.styl';

interface NavbarProps {
  onLogoutHandler: () => void;
}

export const Navbar = (props: NavbarProps) => {
  const { onLogoutHandler } = props;

  return <nav>
    <span className={'logo'}>Simple <b>Issue</b> Tracker</span>
    <button onClick={onLogoutHandler} className={'logOut'}>Logout</button>
  </nav>;
};
