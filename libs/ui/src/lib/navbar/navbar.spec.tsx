import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Navbar } from './navbar';

describe('Navbar', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should render', () => {
    const onLogoutHandler = jest.fn();

    act(() => {
      ReactDOM.render(<Navbar onLogoutHandler={onLogoutHandler} />, container);
    });

    const button = container.querySelector('button');
    const span = container.querySelector('span');

    expect(span.textContent).toBe('Simple Issue Tracker');
    expect(button.textContent).toBe('Logout');

    act(() => {
      button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(onLogoutHandler).toHaveBeenCalled();
  });

});
