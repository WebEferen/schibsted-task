import React, { useEffect, useState } from 'react';
import { Modal } from '../modal';

import './toast-message.styl';

interface ToastMessageProps {
  delayInMs: number;
  toast?: { message: string, type: string }
}

export const ToastMessage = (props: ToastMessageProps) => {
  const { delayInMs, toast } = props;
  const [isVisible, setIsVisible] = useState<boolean>(false);

  useEffect(() => {
    if (toast) {
      setIsVisible(true);
      const hideDelay = setTimeout(() => setIsVisible(false), delayInMs);
      return () => clearTimeout(hideDelay);
    }
  }, [delayInMs, toast]);

  return toast && isVisible ? <Modal id={'toast-message'} isVisible={isVisible} isCentered={true} isTopped={true}>
    <div className={`toast ${toast.type}`}>{ toast.message }</div>;
  </Modal> : null;


};
