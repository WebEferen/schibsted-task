import { PropsWithChildren, useEffect } from 'react';
import * as ReactDOM from 'react-dom';
import './modal.styl';

const modalRoot = document.getElementById('modal-root');

interface ModalProps {
  id: string;
  isVisible: boolean;
  isShadowed?: boolean;
  isCentered?: boolean;
  isTopped?: boolean;
}

export const Modal = (props: PropsWithChildren<ModalProps>) => {
  const { isVisible, children, id, isShadowed, isCentered, isTopped } = props;
  const container = document.createElement('div');

  useEffect(() => {
    if (isVisible) {
      container.classList.add(`modal`, id, isShadowed && 'shadowed', isCentered && 'centered', isTopped && 'topped' );
      modalRoot.appendChild(container);

      return () => modalRoot.removeChild(container);
    }
  }, [isVisible, isCentered, isShadowed, isTopped, id, container]);

  return isVisible ? ReactDOM.createPortal(children, container) : null;
};
